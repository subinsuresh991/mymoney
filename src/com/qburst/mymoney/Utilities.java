package com.qburst.mymoney;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

	@SuppressLint("SimpleDateFormat")
	public static String DateStringForDisplay(String dateString) throws ParseException {
		
		SimpleDateFormat  currentFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");  
		Date date = currentFormat.parse(dateString);
		SimpleDateFormat showFormat = new SimpleDateFormat("dd-MMM hh:mm a");
		return showFormat.format(date);
		
		
	}
	public static boolean isEmail(String email)
	{
	    boolean matchFound1;
	    boolean returnResult=true;
	    email=email.trim();
	    if(email.equalsIgnoreCase(""))
	        returnResult=false;
	    else if(!Character.isLetter(email.charAt(0)))
	        returnResult=false;
	    else 
	    {
	        Pattern p1 = Pattern.compile("^\\.|^\\@ |^_");
	        Matcher m1 = p1.matcher(email.toString());
	        matchFound1=m1.matches();

	        Pattern p = Pattern.compile("^[a-zA-z0-9._-]+[@]{1}+[a-zA-Z0-9]+[.]{1}+[a-zA-Z]{2,4}$");
	        // Match the given string with the pattern
	        Matcher m = p.matcher(email.toString());

	        // check whether match is found
	        boolean matchFound = m.matches();

	        StringTokenizer st = new StringTokenizer(email, ".");
	        String lastToken = null;
	        while (st.hasMoreTokens()) 
	        {
	            lastToken = st.nextToken();
	        }
	        if (matchFound && lastToken.length() >= 2
	        && email.length() - 1 != lastToken.length() && matchFound1==false) 
	        {

	           returnResult= true;
	        }
	        else returnResult= false;
	    }
	    return returnResult;
	 }
	public static void callUser(String phoneNumber, Context context) {
		
		 String uri = "tel:" + phoneNumber.trim() ;
		 Intent intent = new Intent(Intent.ACTION_CALL);
		 intent.setData(Uri.parse(uri));
		 ((Activity)context).startActivity(intent);
	}
	public static boolean isNetworkConnectionAvailable(Context context) {  
	    ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo info = cm.getActiveNetworkInfo();     
	    if (info == null) return false;
	    State network = info.getState();
	    return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
	} 
}
