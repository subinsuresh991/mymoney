package com.qburst.mymoney;


import com.haibison.android.lockpattern.LockPatternActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends Activity {

	private static final int REQ_ENTER_PATTERN = 2;
	private static final int REQ_CHANGE_PATTERN = 6;
	private static final int REQ_CREATE_PATTERN = 1;
	Switch swth_pattern_enable;
	Button btn_change_pattern;
	SharedPreferences prefs;
	private Object str;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		prefs = getSharedPreferences("AppNameSettings", 0);
		boolean isPatternEnabled = prefs.getBoolean("PATTERN_ENBALE", false);
		
		
		swth_pattern_enable = (Switch) findViewById(R.id.switch_pattern_enable);
		btn_change_pattern = (Button) findViewById(R.id.change_pattern);
		
		btn_change_pattern.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				String patternString = prefs.getString("PATTERN_STRING","");
//				
//				
//			    char[] savedPattern  =	patternString.toCharArray();
//				Intent intent = new Intent(LockPatternActivity.ACTION_COMPARE_PATTERN, null,
//				        SettingsActivity.this, LockPatternActivity.class);
//				intent.putExtra(LockPatternActivity.EXTRA_PATTERN, savedPattern);
//				startActivityForResult(intent, REQ_CHANGE_PATTERN);
				
				
				Intent intent = new Intent(SettingsActivity.this, SetSecurityQuestionsActivity.class);
				startActivity(intent);
				
			}
		});
		
		
		swth_pattern_enable.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean isChecked = swth_pattern_enable.isChecked();
				 if(isChecked){
				    	
				    	Intent intent = new Intent(LockPatternActivity.ACTION_CREATE_PATTERN, null,
				    	        SettingsActivity.this, LockPatternActivity.class);
				    	startActivityForResult(intent, REQ_CREATE_PATTERN);
				    	
				    }else{
				    
				    	String patternString = prefs.getString("PATTERN_STRING","");
						
						
					    char[] savedPattern  =	patternString.toCharArray();
						Intent intent = new Intent(LockPatternActivity.ACTION_COMPARE_PATTERN, null,
						        SettingsActivity.this, LockPatternActivity.class);
						intent.putExtra(LockPatternActivity.EXTRA_PATTERN, savedPattern);
						startActivityForResult(intent, REQ_ENTER_PATTERN);
						
						
				    	
				    }
			}
		});
		
		
		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	        Intent data) {
	    switch (requestCode) {
	    case REQ_CREATE_PATTERN: {
	        if (resultCode == RESULT_OK) {
	            char[] pattern = data.getCharArrayExtra(
	                    LockPatternActivity.EXTRA_PATTERN);
	            String str = String.valueOf(pattern);
	            SharedPreferences.Editor ed = prefs.edit();
	            ed.putString("PATTERN_STRING", str);
	            ed.putBoolean("PATTERN_ENBALE", true);
	            ed.commit();
	            Toast.makeText(SettingsActivity.this, "New Lock Pattern Enabled", Toast.LENGTH_LONG).show();
	            
	            
	        }
	        
	        break;
	    }
	    case REQ_ENTER_PATTERN: {
	        if (resultCode == RESULT_OK) {
	            
	        	 SharedPreferences.Editor ed = prefs.edit();
		            ed.putBoolean("PATTERN_ENBALE", false);
		            ed.commit();
		            Toast.makeText(SettingsActivity.this, "Lock Pattern Disabled", Toast.LENGTH_LONG).show();
	            
	        }
	        
	        break;
	    }// REQ_CREATE_PATTERN
	    
	    case REQ_CHANGE_PATTERN: {
	        if (resultCode == RESULT_OK) {
	            
	        	Intent intent = new Intent(LockPatternActivity.ACTION_CREATE_PATTERN, null,
		    	        SettingsActivity.this, LockPatternActivity.class);
		    	startActivityForResult(intent, REQ_CREATE_PATTERN);
	            
	        }
	        
	        break;
	    }
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		boolean isPatternEnabled = prefs.getBoolean("PATTERN_ENBALE", false);
		if(isPatternEnabled)
		{
			swth_pattern_enable.setChecked(true);
			btn_change_pattern.setEnabled(true);
		}
		else{
			
			swth_pattern_enable.setChecked(false);
			btn_change_pattern.setEnabled(false);
		}
		super.onResume();
	}
}
