package com.qburst.mymoney;



import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.haibison.android.lockpattern.LockPatternActivity;

public class SplashScreenActivity extends Activity {

	private static final int REQ_ENTER_PATTERN = 2;
	private static int SPLASH_TIME_OUT = 700;
	SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
 
        new Handler().postDelayed(new Runnable() {
 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
 
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
            	
            	checkForPattern();

            }
        }, SPLASH_TIME_OUT);
    }

    private void checkForPattern() {
    	
    	
    	prefs = getSharedPreferences("AppNameSettings", 0);
    	boolean isPatternEnabled = prefs.getBoolean("PATTERN_ENBALE", false);
    	String patternString = prefs.getString("PATTERN_STRING","");
		if(isPatternEnabled && !patternString.equalsIgnoreCase(""))
		{
		
	    char[] savedPattern  =	patternString.toCharArray();
		Intent intent = new Intent(LockPatternActivity.ACTION_COMPARE_PATTERN, null,
		        this, LockPatternActivity.class);
		intent.putExtra(LockPatternActivity.EXTRA_PATTERN, savedPattern);
		startActivityForResult(intent, REQ_ENTER_PATTERN);
		
		
		}
		else{
			
			Intent i = new Intent(SplashScreenActivity.this, UserListActivity.class);
            startActivity(i);

            // close this activity
            finish();
		}
    }
    
    protected void onActivityResult(int requestCode, int resultCode,
	        Intent data) {
	    switch (requestCode) {
	    case REQ_ENTER_PATTERN: {
	        /*
	         * NOTE that there are 4 possible result codes!!!
	         */
	        switch (resultCode) {
	        case RESULT_OK:
	            // The user passed
	        	
              Intent i = new Intent(SplashScreenActivity.this, UserListActivity.class);
              startActivity(i);

              // close this activity
              finish();
	        	
	            break;
	        case RESULT_CANCELED:
	            // The user cancelled the task
	        	finish();
	            break;
	        case LockPatternActivity.RESULT_FAILED:
	            // The user failed to enter the pattern
	        	finish();
	            break;
	        case LockPatternActivity.RESULT_FORGOT_PATTERN:
	            // The user forgot the pattern and invoked your recovery Activity.
	            break;
	        }

	        /*
	         * In any case, there's always a key EXTRA_RETRY_COUNT, which holds
	         * the number of tries that the user did.
	         */
	        int retryCount = data.getIntExtra(
	                LockPatternActivity.EXTRA_RETRY_COUNT, 0);

	        break;
	    }// REQ_ENTER_PATTERN
	    }
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return false;
		}
		return super.onOptionsItemSelected(item);
	}
}
