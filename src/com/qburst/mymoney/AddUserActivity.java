package com.qburst.mymoney;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.R.array;
import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AddUserActivity extends Activity {

	Button add_button;
	Button contact_list_button;
	EditText user_name;
	EditText phone_number;
	ImageButton userImageButton;
	boolean hasImage;
	boolean isAdding;
	User newUser;
	private Uri uriContact;
    private String contactID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add__user_);
        
		isAdding = getIntent().getBooleanExtra("Adding_User", true);
		
		
		
		add_button = (Button) findViewById(R.id.addButton);
		contact_list_button = (Button) findViewById(R.id.contactButton);
		userImageButton = (ImageButton) findViewById(R.id.userImageButton);
		user_name = (EditText) findViewById(R.id.user_name);
		phone_number = (EditText) findViewById(R.id.phone_number);
		hasImage = false;
		
		final DatabaseHandler db = new DatabaseHandler(this);
		
        if(isAdding){
			
			this.setTitle("Add New User");
			newUser = new User();
		}
		else{
			
			TextView or = (TextView)findViewById(R.id.tv_OR);
			or.setVisibility(View.GONE);
			this.setTitle("Edit User");
			newUser = (User) getIntent().getSerializableExtra("USER_DETAIL");
			add_button.setText("Save");
			contact_list_button.setVisibility(View.GONE);
			user_name.setText(newUser.getUserName());
			phone_number.setText(newUser.getPhoneNumber());
			hasImage = true;
			ByteArrayInputStream imageStream = new ByteArrayInputStream(newUser.imageInByte);
	        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
	        userImageButton.setImageBitmap(theImage);
			
			
		}
		contact_list_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), 1);
			}
		});
		
		add_button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				if(user_name.getText().toString().equals("")) {

					Toast.makeText(AddUserActivity.this, "User name missing", Toast.LENGTH_LONG).show();
				}
				else if(phone_number.getText().toString().equals("")) {

					Toast.makeText(AddUserActivity.this, "Phone number missing", Toast.LENGTH_LONG).show();
				}
				else if(!hasImage) {

					Toast.makeText(AddUserActivity.this, "image missing", Toast.LENGTH_LONG).show();
				}
				else {

					newUser.setUserName(user_name.getText().toString());
					newUser.setPhoneNumber(phone_number.getText().toString());
					if(isAdding){
						
						newUser.setAmount((float)0.0);
						db.addContact(newUser);
						Toast.makeText(AddUserActivity.this, "User added to the list", Toast.LENGTH_SHORT).show();
						
					}
					else{
						
						db.updateContact(newUser);
						Toast.makeText(AddUserActivity.this, "User info updated", Toast.LENGTH_SHORT).show();
						finish();
					}
					
					
					
					
					
					user_name.setText("");
					phone_number.setText("");
					userImageButton.setImageResource(R.drawable.placeholder);
					hasImage = false;
					List<User> contacts = db.getAllUsers();       
					
			        for (User cn : contacts) {
			            String log = "Id: "+cn.getUserID()+" ,Name: " + cn.getUserName() + " ,Phone: " + cn.getPhoneNumber();
			                // Writing Contacts to log
			        Log.d("Name: ", log);
			        
			    }
				}


			}
		});
    
		userImageButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				String [] options ={"Camara","Gallery"};

				AlertDialog.Builder builder = new AlertDialog.Builder(AddUserActivity.this);
			    builder.setTitle("Select Image From")
			           .setItems(options, new DialogInterface.OnClickListener() {
			               public void onClick(DialogInterface dialog, int which) {
			               
			            	  switch (which) {
							case 0:
							      openCamera();
								break;
							case 1:
								  openGallery();
								break;

							default:
								break;
							} 
			            	   
			           }
			    });
			    
			    builder.show();

			}

		});


	}
	 @Override
	   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	      // TODO Auto-generated method stub
		 Bitmap bp = null;
		 ByteArrayOutputStream stream;
		 super.onActivityResult(requestCode, resultCode, data);
		 if(resultCode == RESULT_OK){
		 switch (requestCode) {
		case 0:
			  bp = (Bitmap) data.getExtras().get("data");
		      
		       stream = new ByteArrayOutputStream();
			   bp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				 try{
					 
					 newUser.imageInByte = stream.toByteArray();
					 userImageButton.setImageBitmap(bp);
					 hasImage = true;
				 }catch(Exception e){
					 Toast.makeText(AddUserActivity.this, "Unable to load image", Toast.LENGTH_SHORT).show();
				 }
				 
		      
			break;
        case 1234:
        	 
        	
        		
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();


                bp = BitmapFactory.decodeFile(filePath);
               
                 stream = new ByteArrayOutputStream();
       		    bp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
       		 try{
       			 userImageButton.setImageBitmap(bp);
				 newUser.imageInByte = stream.toByteArray();
				 hasImage = true;
			 }catch(Exception e){
				 
				 //Toast.makeText(AddUserActivity.this, "Unable to load image", Toast.LENGTH_SHORT).show();
			 }
       		    
            
			break;
			
        case 1: 
            Log.d("COntact data", "Response: " + data.toString());
            uriContact = data.getData();
 
           user_name.setText(retrieveContactName());
           phone_number.setText(retrieveContactNumber());
           retrieveContactPhoto();
 
       
		default:
			break;
		}
		 
	 }

		 
	      
	   }
	 
	 private void retrieveContactPhoto() {
		 
	        Bitmap photo = null;
	 
	        try {
	            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
	                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));
	 
	            if (inputStream != null) {
	                
	            	photo = BitmapFactory.decodeStream(inputStream);
	                userImageButton.setImageBitmap(photo);
	                ByteArrayOutputStream stream = new ByteArrayOutputStream();
	                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
	       		    newUser.imageInByte = stream.toByteArray();
	       		    hasImage = true;
	               
	            }
	 
	            assert inputStream != null;
	            inputStream.close();
	 
	        } catch (Exception e) {
	        	Toast.makeText(AddUserActivity.this, "Unable to load image", Toast.LENGTH_SHORT).show();
	            e.printStackTrace();
	        }
	 
	    }
	 
	    private String retrieveContactNumber() {
	 
	        String contactNumber = null;
	 
	        // getting contacts ID
	        Cursor cursorID = getContentResolver().query(uriContact,
	                new String[]{ContactsContract.Contacts._ID},
	                null, null, null);
	 
	        if (cursorID.moveToFirst()) {
	 
	            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
	        }
	 
	        cursorID.close();
	 
	        Log.d("Contact ID", "Contact ID: " + contactID);
	 
	        // Using the contact ID now we will get contact phone number
	        Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
	                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
	 
	                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
	                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
	                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
	 
	                new String[]{contactID},
	                null);
	 
	        if (cursorPhone.moveToFirst()) {
	            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	        }
	 
	        cursorPhone.close();
	 
	        Log.d("Contact number", "Contact Phone Number: " + contactNumber);
	        return contactNumber;
	    }
	 
	    private String retrieveContactName() {
	 
	        String contactName = "";
	 
	        // querying contact data store
	        Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);
	 
	        if (cursor.moveToFirst()) {
	 
	            // DISPLAY_NAME = The display name for the contact.
	            // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.
	        	try {
	        		contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	        	    } catch (Exception e) {

	        	     Log.e("your app", e.toString());
	        	     
	        	    }
	            
	        }
	        if(contactName == null){
	        	
	        	return "";
	        }
	        cursor.close();
	 
	       
	        if(Utilities.isEmail(contactName)){
	        	
	        	if (null != contactName && contactName.length() > 0 )
	        	{
	        	    int endIndex = contactName.lastIndexOf("@");
	        	    if (endIndex != -1)  
	        	    {
	        	    	contactName = contactName.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
	        	    }
	        	}  
	        }
	        return contactName;
	    }
	  
	   private void openCamera() {
     	
		  
			    if (this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
			        // this device has a camera
			    	
			        	Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			            startActivityForResult(intent, 0);
			        
			        
			    } else {
			        // no camera on this device
			    	Toast.makeText(AddUserActivity.this, "No Camera Available", Toast.LENGTH_SHORT).show();
			        
			    }
			   
			}

	   private void openGallery() {
     		
     		Intent i = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			
			startActivityForResult(i, 1234);

     	}
     
}

