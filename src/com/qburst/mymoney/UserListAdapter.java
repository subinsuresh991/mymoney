package com.qburst.mymoney;


import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 
public class UserListAdapter extends BaseAdapter {
 
    private Activity activity;
  //  private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
    ArrayList<User> userList;
    ArrayList<User> filteredList;
    private static LayoutInflater inflater=null;
    private String              _query;
    private ForegroundColorSpan _querySpan;
    private Filter filter;
    DatabaseHandler db;
 
    public UserListAdapter(Activity a, ArrayList<User> list) {
        activity = a;
        filteredList = list;
        userList = list;
        _query = "";
        _querySpan = new ForegroundColorSpan(Color.GREEN);
        db = new DatabaseHandler(activity);
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       // imageLoader=new ImageLoader(activity.getApplicationContext());
    }
 
    public int getCount() {
        return filteredList.size();
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
 
    public void setData(ArrayList<User> list){
        userList = list;
        filteredList = list;
        notifyDataSetChanged();
      }
      
      public String getQuery(){
        return _query;
      }
      
      public void setQuery(String query){
        _query = query;
         getFilter().filter(_query);
        notifyDataSetChanged();
      }
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.user_list_item, null);
 
        TextView user_name = (TextView)vi.findViewById(R.id.username_tv); // title
        TextView amount = (TextView)vi.findViewById(R.id.tv_amount);
        TextView phone = (TextView)vi.findViewById(R.id.tv_phone);
        TextView time = (TextView)vi.findViewById(R.id.tv_time);
        ImageView userimg = (ImageView)vi.findViewById(R.id.img_user);// artist name

        User user = filteredList.get(position);
       
        	user_name.setText(user.getUserName());
        	float tempAmount = user.getAmount();
            if(tempAmount < 0) {
            	
            	amount.setTextColor(Color.RED);
            	tempAmount = tempAmount* -1;
            }
            else {
            	amount.setTextColor(Color.GREEN);
    		}
            amount.setText("\u20B9 " +Float.toString(tempAmount));
            phone.setText(user.getPhoneNumber());
            
            try {
    			time.setText(Utilities.DateStringForDisplay(user.time).toString());
    		} catch (ParseException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
           
            ByteArrayInputStream imageStream = new ByteArrayInputStream(user.imageInByte);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            userimg.setImageBitmap(theImage);
          int index = TextUtils.isEmpty(_query) ? -1 : user.getUserName().toLowerCase().indexOf(_query.toLowerCase());
             if( index == -1 ){
            
        }else{
        	
          SpannableStringBuilder ssb = new SpannableStringBuilder(user.getUserName());
          ssb.setSpan(_querySpan, index, index + _query.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
          user_name.setText(ssb);
        }
         
             
             vi.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					User user = filteredList.get(position);
					Intent intent = new Intent(activity, UserDetailViewActivity.class);
					intent.putExtra("USER_DETAIL", user);
					activity.startActivity(intent);
					
					
				}
			});
             vi.setLongClickable(true) ;
             
             vi.setOnLongClickListener(new View.OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					
					
					showAlertDialogue(position);
					return true;
				}
			});
             
             
             
        return vi;
    }
    
    
    
    



private void showAlertDialogue(final int position) {
	
	AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

	// Setting Dialog Title
	alertDialog.setTitle("Confirm Delete...");

	// Setting Dialog Message
	alertDialog.setMessage("Are you sure you want delete this?");

	// Setting Icon to Dialog
	//alertDialog.setIcon(R.drawable.delete);

	// Setting Positive "Yes" Button
	alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog,int which) {

			User user = filteredList.get(position);
			db.deleteContact(user);
			filteredList.remove(position);
			userList.remove(user);
			notifyDataSetChanged();
		    Toast.makeText(activity, "User removerd from the list", Toast.LENGTH_SHORT).show();
		}
	});

	// Setting Negative "NO" Button
	alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog,	int which) {
		// Write your code here to invoke NO event
		
			
		dialog.cancel();
		}
	});

	// Showing Alert Message
	alertDialog.show();
	
	
}
    
	public Filter getFilter() {
		return new UserListFilter();
	}

	public class UserListFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub

			FilterResults Result = new FilterResults();
			// if constraint is empty return the original names
			if (_query.length() == 0) {
				Result.values = userList;
				Result.count = userList.size();

				return Result;
			}

			ArrayList<User> Users = new ArrayList<User>();
			
			 User filterableUser;

			for (int i = 0; i < userList.size(); i++) {
				filterableUser = userList.get(i);
				if (filterableUser.getUserName().toLowerCase()
						.contains(_query)) {
					Users.add(filterableUser);
				}
			}
			Result.values = Users;
			Result.count = Users.size();
			return Result;
		}

		public FilterResults performFilteringPub(CharSequence constraint) {
			return performFiltering(constraint);
		}

		public void publishResultsPub(CharSequence constraint,
				FilterResults results) {
			publishResults(constraint, results);
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			filteredList = (ArrayList<User>) results.values;
			if(filteredList.size() == 0 && constraint.length()>0){
				((UserListActivity)activity).showNoResult(true);
				
			}else{
				
				((UserListActivity)activity).showNoResult(false);
			}
			notifyDataSetChanged();

		}
	}
    
}