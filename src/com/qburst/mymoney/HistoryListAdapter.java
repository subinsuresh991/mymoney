package com.qburst.mymoney;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryListAdapter extends BaseAdapter{
      
	
	 private Activity activity;
	  //  private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
	    ArrayList<History> HistoryList;
	    private static LayoutInflater inflater=null;
	   
	 
	    public HistoryListAdapter(Activity a, ArrayList<History> list) {
	        activity = a;
	        HistoryList = list;
	        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	       // imageLoader=new ImageLoader(activity.getApplicationContext());
	    }
	 
	    public int getCount() {
	        return HistoryList.size();
	    }
	 
	    public Object getItem(int position) {
	        return position;
	    }
	 
	    public long getItemId(int position) {
	        return position;
	    }
	 
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View vi=convertView;
	        if(convertView==null)
	            vi = inflater.inflate(R.layout.history_list_item, null);
	 
	        TextView hintText = (TextView)vi.findViewById(R.id.tv_hint); // title
	        TextView amount = (TextView)vi.findViewById(R.id.tv_amount);
	        TextView time = (TextView)vi.findViewById(R.id.tv_date);

	        History hintItem = HistoryList.get(position);
	       
	        hintText.setText(hintItem.getHint());
	        if(hintItem.getIsLend() == 1) {
	        	
	        	amount.setTextColor(Color.GREEN);
	        }
	        else {
	        	amount.setTextColor(Color.RED);
			}
	        amount.setText("\u20B9 "+Float.toString(hintItem.getAmount()));
	       
	        try {
				time.setText(Utilities.DateStringForDisplay(hintItem.time).toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	 
	        return vi;
	    }
}
