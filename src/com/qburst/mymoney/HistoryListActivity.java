package com.qburst.mymoney;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class HistoryListActivity extends Activity {

	ArrayList<History> historyList;
	HistoryListAdapter adapter;
	ListView historyListView;
	User selectedUser;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_histoy_list);
		
		
		Intent intent = getIntent();
        selectedUser = (User) intent.getSerializableExtra("USER_DETAIL");
        historyListView = (ListView) findViewById(R.id.list_history);
        if(selectedUser == null) {
        	
        	Toast.makeText(HistoryListActivity.this, "Error Occured", Toast.LENGTH_LONG).show();	
        	finishActivity(0);
        }
        else {
			
        	DatabaseHandler db = new DatabaseHandler(this);
        	historyList = db.getHistoryForUser(selectedUser);
        	adapter= new HistoryListAdapter(this, historyList);
        	historyListView.setAdapter(adapter);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.histoy_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}
}
