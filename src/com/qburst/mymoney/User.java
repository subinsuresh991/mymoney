package com.qburst.mymoney;
import java.io.Serializable;

import android.R.string;




public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int user_id;
	private String user_name;
	private float amount;
	private String phone_number;
	public String time;
	//private String phone_number;
    public  byte[] imageInByte;
	
	public User() {
		
	}
	
	public User(int id, String username, float amount, String number) {
		this.user_id = id;
		this.user_name = username;
		this.amount = amount;
		this.phone_number = number;
	}
	
	public User(String username, float amount, String number) {
		this.user_name = username;
		this.amount = amount;
		this.phone_number = number;
	}
	
	public void setUserID(int id) {
		this.user_id = id;
	}
	
	public int getUserID() {
		return this.user_id;
	}
	
	public void setUserName(String username) {
		this.user_name = username;
	}
	
	public String getUserName() {
		return this.user_name;
	}
	
	public void setPhoneNumber(String number) {
		this.phone_number = number;
	}
	
	public String getPhoneNumber() {
		return this.phone_number;
	}
	
	public void setAmount(float amount) {
		this.amount = amount;
	}
	
	public float getAmount() {
		return this.amount;
	}
	
	
}