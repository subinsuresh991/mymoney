package com.qburst.mymoney;

import java.io.ObjectOutputStream.PutField;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import android.R.bool;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;

//This is your preferred flag

public class UserListActivity extends Activity {

	public final static String ID_EXTRA = "Extra";
	ListView userListView;
	TextView txt_no_result;
	DatabaseHandler db;
	UserListAdapter adapter;
	public List<User> userList;
	private String _curFilter;
	LinearLayout add_layout;
	boolean islogged;
	/** The view to show the ad. */
	private AdView adView;
	/* Your ad unit id. Replace with your actual ad unit id. */
	private static final String AD_UNIT_ID = "ca-app-pub-8867322576547993/7564486264";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_list);

		userListView = (ListView) findViewById(R.id.user_list);
		db = new DatabaseHandler(this);
		AppRater.app_launched(this);

		txt_no_result = (TextView) findViewById(R.id.txt_no_result);
		userList = db.getAllUsers();
		adapter = new UserListAdapter(this, (ArrayList) userList);
		userListView.setAdapter(adapter);

		// Create the adView.
		adView = new AdView(this);
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		// Add the AdView to the view hierarchy. The view will have no size
		// until the ad is loaded. This code assumes you have a LinearLayout
		// with
		// attribute android:id="@+id/linear_layout" in your activity_main.xml.
		add_layout = (LinearLayout) findViewById(R.id.layout_adview);
		add_layout.addView(adView);
		
		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device.
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice("842825DC986AD6AE3BD086CFB889BD85").build();

		// Start loading the ad in the background.
		adView.loadAd(adRequest);
		adView.setAdListener(new AdListener() {

			@Override
			public void onAdLoaded() {
				super.onAdLoaded();

				add_layout.setVisibility(View.VISIBLE);
			}

			public void onAdFailedToLoad(int errorCode) {

				add_layout.setVisibility(View.GONE);
			}

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_list_menu, menu);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				_curFilter = !TextUtils.isEmpty(newText) ? newText : "";
				adapter.setQuery(_curFilter);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub

				_curFilter = "";
				adapter.setQuery(_curFilter);
				return false;
			}
		});
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		
		if(item.getItemId() == R.id.action_add){
			
			Intent intent = new Intent(this, AddUserActivity.class);
			intent.putExtra("Adding_User", true);
			startActivity(intent);
			return true;
		}
		else if(item.getItemId() == R.id.action_search){
			
			return true;
		}
		else if(item.getItemId() == R.id.action_settings){
			
			Intent settingIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingIntent);
			return true;
		}
		else{
			
			return super.onOptionsItemSelected(item);
		}
//		switch (item.getItemId()) {
//		case R.id.action_add:
//			Intent intent = new Intent(this, AddUserActivity.class);
//			intent.putExtra("Adding_User", true);
//			startActivity(intent);
//
//			return true;
//		case R.id.action_search:
//
//			return true;
//
//		case R.id.action_settings:
//			Intent settingIntent = new Intent(this, SettingsActivity.class);
//			startActivity(settingIntent);
//			return true;
//
//		default:
			
//		}
	}

	/**
	 * Callback method from {@link UserListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		userList = db.getAllUsers();
		adapter = new UserListAdapter(this, (ArrayList) userList);
		userListView.setAdapter(adapter);
		if (_curFilter != null && !_curFilter.equalsIgnoreCase("")) {

			adapter.setQuery(_curFilter);
		}
		if (adView != null && Utilities.isNetworkConnectionAvailable(this)) {
			adView.resume();

			add_layout.setVisibility(View.VISIBLE);

		} else {

			add_layout.setVisibility(View.GONE);
		}

	}

	public void showNoResult(boolean show) {

		if (show) {

			txt_no_result.setVisibility(View.VISIBLE);
			userListView.setVisibility(View.INVISIBLE);

		} else {

			txt_no_result.setVisibility(View.INVISIBLE);
			userListView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
}
