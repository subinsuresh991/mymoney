package com.qburst.mymoney;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class AddUserWidgetProvider extends AppWidgetProvider {

	

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.add_user_widget);
		remoteView.setOnClickPendingIntent(R.id.widget_icon, buttonPendingIntent(context));
		appWidgetManager.updateAppWidget(appWidgetIds, remoteView);
	}
	
	private PendingIntent buttonPendingIntent(Context context) {
		// TODO Auto-generated method stub
		
		 Intent intent = new Intent(context, AddUserActivity.class);
         PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
         
		return pendingIntent;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	}
}
