package com.qburst.mymoney;

import java.io.Serializable;




public class History implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int user_id;
	private String hint;
	private float amount;
	private int isLend;
	private int hint_id;
 
	public String time;
	public History() {
		
	}
	
	public History(int id, String hint, float amount, int islend, int hintId) {
		this.user_id = id;
		this.hint = hint;
		this.amount = amount;
		this.isLend = islend;
		this.hint_id = hintId;
	}
	
	public History(int id, String hint, float amount, int islend) {
		this.user_id = id;
		this.hint = hint;
		this.amount = amount;
		this.isLend = islend;
	}
	
	public void setUserID(int id) {
		this.user_id = id;
	}
	
	public int getUserID() {
		return this.user_id;
	}
	
	public void setHint(String hint) {
		this.hint = hint;
	}
	
	public String getHint() {
		return this.hint;
	}
	
	public void setIsLend(int isLand) {
		this.isLend = isLand;
	}
	
	public int getIsLend() {
		
		return this.isLend;
	}
	public int getHintId() {
		return this.hint_id;
	}
	
	public void setHintId(int id) {
		 this.hint_id = id;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	
	public float getAmount() {
		return this.amount;
	}

	
	
	
}