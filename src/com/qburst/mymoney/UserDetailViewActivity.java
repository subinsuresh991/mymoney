package com.qburst.mymoney;

import java.io.ByteArrayInputStream;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class UserDetailViewActivity extends Activity {

	int userid;
	TextView tv_username;
	TextView tv_phoneno;
	TextView tv_amount;
	EditText ed_hint;
	EditText ed_amount;
	Button btn_history;
	Button btn_done;
	ImageView img_userimage;
	RadioButton rb_lend;
	RadioButton rb_borrow;
	User selectedUser;
	DatabaseHandler db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_detail_view);
		
		db = new DatabaseHandler(this);
		
		Intent intent = getIntent();
        selectedUser = (User) intent.getSerializableExtra("USER_DETAIL");
        userid = selectedUser.getUserID();
        tv_username = (TextView)findViewById(R.id.username);
        tv_phoneno = (TextView)findViewById(R.id.phonenumber);
        tv_amount = (TextView)findViewById(R.id.amount);
        ed_hint = (EditText)findViewById(R.id.hint);
        ed_amount = (EditText)findViewById(R.id.value);
        img_userimage = (ImageView)findViewById(R.id.userimage);
        btn_done = (Button)findViewById(R.id.done);
        btn_history = (Button)findViewById(R.id.history);
        rb_lend = (RadioButton)findViewById(R.id.lend);
        rb_borrow = (RadioButton)findViewById(R.id.borrow);
        
        
        btn_history.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				
				Intent intent = new Intent(UserDetailViewActivity.this, HistoryListActivity.class);
				intent.putExtra("USER_DETAIL", selectedUser);
				startActivity(intent);
				
			}
			});
        btn_done.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if(ed_hint.getText().toString().equals("")) {

					Toast.makeText(UserDetailViewActivity.this, "Enter some hint", Toast.LENGTH_LONG).show();
				}
				else if(ed_amount.getText().toString().equals("")) {

					Toast.makeText(UserDetailViewActivity.this, "amount missing", Toast.LENGTH_LONG).show();
				}
				else  {

					float amount = selectedUser.getAmount();
					History hintItem = new History();
					hintItem.setUserID(selectedUser.getUserID());
					hintItem.setHint(ed_hint.getText().toString());
					hintItem.setAmount( Float.valueOf(ed_amount.getText().toString()));
					if(rb_lend.isChecked()) {
						
						hintItem.setIsLend(1);
						amount = amount + hintItem.getAmount();
					}
					else {
						
						hintItem.setIsLend(0);
						amount = amount - hintItem.getAmount();
					}
					
					selectedUser.setAmount(amount);
					
					db.addHistory(hintItem);
					db.updateContact(selectedUser);
					Toast.makeText(UserDetailViewActivity.this, "Updated", Toast.LENGTH_SHORT).show();
					ed_hint.setText("");
					ed_amount.setText("");
					setAmount();
				}
				
				
				
			}

		});
      tv_phoneno.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			
			Utilities.callUser(selectedUser.getPhoneNumber(),UserDetailViewActivity.this);
			
		}
	});

	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.user_detail_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    
	    
	    if(item.getItemId() == R.id.action_edit){
	    	
	    	Intent intent = new Intent(this, AddUserActivity.class);
        	intent.putExtra("Adding_User", false);
        	intent.putExtra("USER_DETAIL", selectedUser);
        	startActivity(intent);
             return true;
	    }
	    else{
	    	
	    	 return super.onOptionsItemSelected(item);
	    }
	}
	private void setAmount() {
		
		float tempAmount = selectedUser.getAmount();
		if(tempAmount < 0) {
        	
        	tv_amount.setTextColor(Color.RED);
        	tempAmount = tempAmount * -1;
        }else {
        	tv_amount.setTextColor(Color.GREEN);
		}
		
		tv_amount.setText("\u20B9 "+Float.toString(tempAmount));
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		
		selectedUser = db.getUser(userid);
		tv_username.setText(selectedUser.getUserName());
        tv_phoneno.setText(selectedUser.getPhoneNumber());
        tv_amount.setText(Float.toString(selectedUser.getAmount()));
        setAmount();
        
        ByteArrayInputStream imageStream = new ByteArrayInputStream(selectedUser.imageInByte);
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        img_userimage.setImageBitmap(theImage);
		super.onResume();
	}
}
