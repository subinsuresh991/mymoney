package com.qburst.mymoney;


 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
 
import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ParseException;
import android.util.Log;
import android.widget.Toast;
 
public class DatabaseHandler extends SQLiteOpenHelper {
 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "usermanager";
 
    // Contacts table name
    private static final String TABLE_USERS = "userslist";
    private static final String TABLE_HISTORY = "historylist";
    
 
    // Contacts Table Columns names
    private static final String KEY_ID = "user_id";
    private static final String KEY_NAME = "user_name";
    private static final String KEY_PH_NO = "phone_number";
    private static final String KEY_AMOUNT = "amount";
    
    private static final String KEY_HISTORY_ID = "history_id";
    private static final String KEY_HINT = "hint";
    private static final String KEY_ISLEND = "isLend";
    private static final String KEY_TIME = "time";
    private static final String KEY_IMAGE = "image";
    
   // private static final String KEY_PH_NO = "phone_number";
    
    
 
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT," + KEY_AMOUNT + " REAL," + KEY_TIME + " TEXT," + KEY_IMAGE + " REAL" +")";
        Log.v(KEY_NAME, CREATE_CONTACTS_TABLE);
        db.execSQL(CREATE_CONTACTS_TABLE);
        
        
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + KEY_HISTORY_ID + " INTEGER PRIMARY KEY," + KEY_HINT + " TEXT,"
                + KEY_ID + " INTEGER," + KEY_AMOUNT + " REAL," + KEY_ISLEND + " INTEGER," + KEY_TIME + " TEXT" +")";
        Log.v(KEY_NAME, CREATE_HISTORY_TABLE);
        db.execSQL(CREATE_HISTORY_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        // Create tables again
        onCreate(db);
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
 
    // Adding new contact
    void addContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        //Log.v(KEY_NAME, user.getUserName());
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, user.getUserName()); // Contact Name
        values.put(KEY_PH_NO, user.getPhoneNumber()); // Contact Phone
        values.put(KEY_AMOUNT, user.getAmount());
        values.put(KEY_TIME, getDateTime());
        values.put(KEY_IMAGE, user.imageInByte);
        db.insert(TABLE_USERS, null, values);
        db.close(); // Closing database connection
    }
    void addHistory(History hintItem) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        //Log.v(KEY_NAME, user.getUserName());
        ContentValues values = new ContentValues();
        values.put(KEY_HINT, hintItem.getHint()); // Contact Name
        values.put(KEY_ISLEND,hintItem.getIsLend() ); // Contact Phone
        values.put(KEY_AMOUNT, hintItem.getAmount());
        values.put(KEY_ID, hintItem.getUserID());
        values.put(KEY_TIME, getDateTime());
        db.insert(TABLE_HISTORY, null, values);
        db.close(); // Closing database connection
    }
    // Getting single contact
   public User getUser(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
//        Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID,
//                KEY_NAME, KEY_PH_NO, KEY_AMOUNT }, KEY_ID + "=?",
//                new String[] { String.valueOf(id) }, null, null, null, null);
        User user = new User();
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
          if(cursor.moveToFirst()){
            	
        	  do{
        		  int userid = Integer.parseInt(cursor.getString(0));
            	  if(userid == id){
            		  
            		  
                      user.setUserID(Integer.parseInt(cursor.getString(0)));
                      user.setUserName(cursor.getString(1));
                      user.setPhoneNumber(cursor.getString(2));
                      user.setAmount(cursor.getFloat(3));
                      user.time = cursor.getString(4);
                      user.imageInByte = cursor.getBlob(5);
                      return user;
            	  }
        	  }while (cursor.moveToNext());
        	  
            }
 
        
        }// return contact
       return user;
    }
     
    public ArrayList<History> getHistoryForUser(User user) {
    	
    	int id = user.getUserID();
    	ArrayList<History>historyList = new ArrayList<History>();
    	SQLiteDatabase db = this.getReadableDatabase();
    	String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;
    	Cursor cursor = db.rawQuery(selectQuery, null);
//        if (cursor != null)
//            cursor.moveToFirst();
        if (cursor.moveToFirst()) {
            do {
                
            	
            	
                // Adding contact to list
            	int userid = Integer.parseInt(cursor.getString(2));
            	if (userid == id) {
            		
            		History hintItem = new History();
                	hintItem.setHintId(Integer.parseInt(cursor.getString(0)));
                	hintItem.setHint(cursor.getString(1));
                	hintItem.setUserID(userid);
                	hintItem.setAmount(cursor.getFloat(3));
                	hintItem.setIsLend(Integer.parseInt(cursor.getString(4)));
                	hintItem.time = cursor.getString(5);
            		historyList.add(hintItem);
            	}
               
            } while (cursor.moveToNext());
        }
        historyList = sortHistoryList(historyList);
        return historyList;
		
	}
    // Getting All Contacts
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setUserID(Integer.parseInt(cursor.getString(0)));
                user.setUserName(cursor.getString(1));
                user.setPhoneNumber(cursor.getString(2));
                user.setAmount(cursor.getFloat(3));
                user.time = cursor.getString(4);
                user.imageInByte = cursor.getBlob(5);
                userList.add(user);
            } while (cursor.moveToNext());
        }
 
        // return contact list
        userList = sortUserList(userList);
        return userList;
    }
 
    // Updating single contact
    public int updateContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, user.getUserName());
        values.put(KEY_PH_NO, user.getPhoneNumber());
        values.put(KEY_AMOUNT, user.getAmount());
        values.put(KEY_TIME, getDateTime());
        // updating row
        return db.update(TABLE_USERS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(user.getUserID()) });
    }
 
    // Deleting single contact
    public void deleteContact(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USERS, KEY_ID + " = ?",
                new String[] { String.valueOf(user.getUserID()) });
        db.close();
    }
 
 
    // Getting contacts Count
    public int getUsersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
 
        // return count
        return cursor.getCount();
    }
    private String getDateTime() {
    	
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MMM-yyyy hh:mm:ss a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
    private  List<User> sortUserList(List<User> userlist) {
    	
    	Collections.sort(userlist, new Comparator<User>(){
    		  public int compare(User user1, User user2) {SimpleDateFormat  format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");  
			  Date date1 = null;
			try {
				date1 = format.parse(user1.time);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      Date date2 = null;
			try {
				date2 = format.parse(user2.time);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
//			  
		    return date2.compareTo(date1);
    		  }
    		});
    	
    return userlist;
   }
    
    private  ArrayList<History> sortHistoryList(ArrayList<History> historyList) {
    	
    	Collections.sort(historyList, new Comparator<History>(){
    		  public int compare(History hint1, History hint2) {
    			  
    			  SimpleDateFormat  format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");  
    			  Date date1 = null;
				try {
					date1 = format.parse(hint1.time);
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			      Date date2 = null;
				try {
					date2 = format.parse(hint2.time);
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
//    			  
    		    return date2.compareTo(date1);
    		  }
    		});
    	
    return historyList;
   }
}